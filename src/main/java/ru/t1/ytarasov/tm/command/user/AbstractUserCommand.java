package ru.t1.ytarasov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.service.IUserService;
import ru.t1.ytarasov.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
