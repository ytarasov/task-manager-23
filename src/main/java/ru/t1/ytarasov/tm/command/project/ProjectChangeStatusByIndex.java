package ru.t1.ytarasov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIndex extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-change-by-index";

    @NotNull
    public static final String DESCRIPTION = "Change status of the PROJECT by index";


    @Override
    public void execute() throws AbstractException {
        System.out.println("[CHANGE STATUS OF THE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @Nullable final String userId = getAuthService().getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
