package ru.t1.ytarasov.tm.exception.field;

public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error! User's role is empty...");
    }

}
