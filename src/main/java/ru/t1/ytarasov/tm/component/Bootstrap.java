package ru.t1.ytarasov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.ICommandRepository;
import ru.t1.ytarasov.tm.api.repository.IProjectRepository;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.api.repository.IUserRepository;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.command.project.*;
import ru.t1.ytarasov.tm.command.system.*;
import ru.t1.ytarasov.tm.command.task.*;
import ru.t1.ytarasov.tm.command.user.*;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ytarasov.tm.exception.system.CommandNotSupportedException;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.repository.CommandRepository;
import ru.t1.ytarasov.tm.repository.ProjectRepository;
import ru.t1.ytarasov.tm.repository.TaskRepository;
import ru.t1.ytarasov.tm.repository.UserRepository;
import ru.t1.ytarasov.tm.service.*;
import ru.t1.ytarasov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        try {
            registry(new ApplicationAboutCommand());
            registry(new ApplicationHelpCommand());
            registry(new ApplicationVersionCommand());
            registry(new CommandListCommand());
            registry(new ArgumentListCommand());
            registry(new ApplicationInfoCommand());
            registry(new ApplicationExitCommand());

            registry(new ProjectCreateCommand());
            registry(new ProjectListCommand());
            registry(new ProjectShowByIdCommand());
            registry(new ProjectShowByIndexCommand());
            registry(new ProjectUpdateByIdCommand());
            registry(new ProjectUpdateByIndexCommand());
            registry(new ProjectRemoveByIdCommand());
            registry(new ProjectRemoveByIndexCommand());
            registry(new ProjectClearCommand());
            registry(new ProjectStartByIdCommand());
            registry(new ProjectStartByIndexCommand());
            registry(new ProjectCompleteByIdCommand());
            registry(new ProjectCompleteByIndexCommand());
            registry(new ProjectChangeStatusById());
            registry(new ProjectChangeStatusByIndex());

            registry(new TaskCreateCommand());
            registry(new TaskListCommand());
            registry(new TaskClearCommand());
            registry(new TaskShowByIdCommand());
            registry(new TaskShowByIndexCommand());
            registry(new TaskFindByProjectIdCommand());
            registry(new TaskUpdateByIdCommand());
            registry(new TaskUpdateByIndexCommand());
            registry(new TaskRemoveByIdCommand());
            registry(new TaskRemoveByIndexCommand());
            registry(new TaskStartByIdCommand());
            registry(new TaskStartByIndexCommand());
            registry(new TaskCompleteByIdCommand());
            registry(new TaskCompleteByIndexCommand());
            registry(new TaskChangeStatusByIdCommand());
            registry(new TaskChangeStatusByIndexCommand());
            registry(new TaskBindToProjectCommand());
            registry(new TaskUnbindFromProjectCommand());

            registry(new UserRegistryCommand());
            registry(new UserChangePasswordCommand());
            registry(new UserLoginCommand());
            registry(new UserLogoutCommand());
            registry(new UserUpdateProfileCommand());
            registry(new UserViewProfileCommand());
            registry(new UserLockCommand());
            registry(new UserUnlockCommand());
            registry(new UserRemoveCommand());
        }
        catch (@Nullable AbstractException e) {
            getLoggerService().error(e);
            System.out.println();
        }
    }

    public void runArguments(@Nullable String[] args) {
        if (args == null || args.length == 0) {
            runCommands();
            return;
        }
        final String arg = args[0];
        try {
            runArgument(arg);
        } catch (@Nullable final AbstractException e) {
            getLoggerService().error(e);
            System.out.println();
        }
    }

    private void runArgument(@Nullable final String arg) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void runCommands() {
        initLogger();
        System.out.println();
        initDemoData();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                @Nullable String command = TerminalUtil.nextLine();
                runCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@Nullable final Exception e) {
                getLoggerService().error(e);
                System.out.println();
                System.out.println("[FAIL]");
            }
        }
    }

    private void runCommand(@Nullable final String command) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void initDemoData() {
        try {
            @Nullable User user1 = userService.create("TEST", "TEST", "test@mail.ru");
            @Nullable User user2 = userService.create("TEST2", "TEST2", "test2@mail.ru");
            @Nullable User userAdmin = userService.create("ADMIN", "ADMIN", Role.ADMIN);

            projectService.create(user1.getId(), "BETA PROJECT", "This is beta project", Status.IN_PROGRESS);
            projectService.create(user1.getId(), "ALPHABET", "This is alphabet project", Status.COMPLETED);
            projectService.create(user1.getId(), "ANT", "This is ant project", Status.NOT_STARTED);
            taskService.create(user1.getId(), "MAIN TASK", "This is main task", Status.IN_PROGRESS);
            taskService.create(user1.getId(), "ZED TASK", "This is zed task", Status.NOT_STARTED);
            taskService.create(user1.getId(), "SECONDARY TASK", "This is secondary task", Status.COMPLETED);
        } catch (final @Nullable AbstractException e) {
            System.out.println(e.getMessage());
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO THE TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void registry(final @NotNull AbstractCommand command) throws AbstractException {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
