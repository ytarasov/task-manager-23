package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.exception.AbstractException;
import ru.t1.ytarasov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    List<M> findAll(@Nullable String userId) throws AbstractException;

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Comparator comparator) throws AbstractException;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    M findOneByIndex(@Nullable String userId, @NotNull Integer index) throws AbstractException;

    int getSize(@NotNull String userId) throws AbstractException;

    @Nullable
    M removeById(@Nullable String userId, @NotNull String id) throws AbstractException;

    @Nullable
    M removeByIndex(@Nullable String userId, int index) throws AbstractException;

    @Nullable
    M add(@Nullable String userId, @NotNull M model) throws AbstractException;

    @Nullable
    M remove(@NotNull String userId, @NotNull M model) throws AbstractException;

}
