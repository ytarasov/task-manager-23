package ru.t1.ytarasov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.repository.ITaskRepository;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        Task task = new Task(name);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId,
                                @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        Task task = new Task(name,status);
        task.setUserId(userId);
        return add(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull String userId, @NotNull String name, @NotNull String description, @NotNull Status status) {
        Task task = new Task(name,description, status);
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public List<Task> findAllTasksByProjectId(@NotNull String userId, final @NotNull String projectId) {
        return models
                .stream()
                .filter(m -> projectId.equals(m.getProjectId()))
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

}
